import logo from './logo.svg';
import './App.css';
import JokeList from './components/joke-list';
import { BrowserRouter, NavLink, Route, Routes } from 'react-router-dom';
import LoginPage from './components/page/login-page';
import JokeRaterPage from './components/page/joke-rater-page';
import JokeReportPage from './components/page/joke-report-page';

function App() {
  return (
    <div className="App">
      <h1>Welcome to Joke Monger</h1>
      <BrowserRouter>
      <NavLink to="/">Login</NavLink>
      <NavLink to="/rater">Rate Jokes</NavLink>
      <NavLink to="/report">Report</NavLink>
      <Routes>
        <Route path='/' element={<LoginPage/>}/>
        <Route path='/rater' element={<JokeRaterPage/>}/>
        <Route path='/report' element={<JokeReportPage/>}/>
      </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
