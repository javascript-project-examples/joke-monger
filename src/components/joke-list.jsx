import { result } from "lodash";
import { useEffect, useState } from "react";
import Joke from "./joke";

function JokeList() {

  const [currentJoke,setCurrentJoke] = useState({})

  useEffect(()=>{
    // on componentDidMount
    fetch('https://v2.jokeapi.dev/joke/Any?blacklistFlags=nsfw,religious,political,racist,sexist,explicit&type=single')
    .then(response => response.json())
    .then(result=>setCurrentJoke(result))
    
  },[])


  return (
    <div>
      <h4>{currentJoke.joke}</h4>
    </div>
  );
}

export default JokeList;
